RTC.SYS and "dot" commands for ESXDOS 0.8.X 

.DATE to get or set the date. Current format is DD/MM/YYYY and minimum date is 01/01/2000 (DS1307 limit)
.TIME to get or set the time in 24 hour format